package com.rave.kotlinbasic

fun displayWeather(city: String, low: Int, high: Int, rainChance: Int) {
    println("City: $city")
    println("Low temperature: $low, High temperature: $high")
    println("Chance of rain: $rainChance%")
    println()
}