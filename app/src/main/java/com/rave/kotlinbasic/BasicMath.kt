package com.rave.kotlinbasic

fun calculateSum(firstNumber: Int, secondNumber: Int) {
    val result = firstNumber+secondNumber
    println("$firstNumber + $secondNumber = $result")
}