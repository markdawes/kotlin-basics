package com.rave.kotlinbasic

fun displayAlert(operatingSystem: String = "Unknown OS", emailId: String = "sample@gmail.com") {
    println("There's a new sign-in request on $operatingSystem for your Google Account $emailId.")
}