package com.rave.kotlinbasic

fun main() {
    displayAlert()
}

fun formatedMessage() {
    val baseSalary = 5000
    val bonusAmount = 1000
    val totalSalary = "$baseSalary + $bonusAmount"
    println("Congratulations for your bonus! You will receive a total of $totalSalary(additional bonus).")
}

/* Will output: 'Congratulations for your bonus! You will receive a total of 5000 + 1000(additional bonus).' */